
/**
 * Redes Integradas de Telecomunicações II MIEEC 2013/2014
 *
 * httpThread.java
 *
 * Class that handles client's requests. It must handle HTTP GET, HEAD and POST
 * client requests INCOMPLETE VERSION
 *
 */
import java.io.*;
import java.net.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class httpThread extends Thread {

    guiHttpd root;
    ServerSocket ss;
    Socket client;
    DateFormat httpformat;

    /**
     * Creates a new instance of httpThread
     */
    public httpThread(guiHttpd root, ServerSocket ss, Socket client) {
        this.root = root;
        this.ss = ss;
        this.client = client;
        httpformat = new SimpleDateFormat("EE, d MMM yyyy HH:mm:ss zz", Locale.UK);
        httpformat.setTimeZone(TimeZone.getTimeZone("GMT"));
        setPriority(NORM_PRIORITY - 1);
    }

    /**
     * The type for unguessable files
     */
    String guessMime(String fn) {
        String lcname = fn.toLowerCase();
        int extenStartsAt = lcname.lastIndexOf('.');
        if (extenStartsAt < 0) {
            if (fn.equalsIgnoreCase("makefile")) {
                return "text/plain";
            }
            return "unknown/unknown";
        }
        String exten = lcname.substring(extenStartsAt);
        // System.out.println("Ext: "+exten);
        if (exten.equalsIgnoreCase(".htm")) {
            return "text/html";
        } else if (exten.equalsIgnoreCase(".html")) {
            return "text/html";
        } else if (exten.equalsIgnoreCase(".gif")) {
            return "image/gif";
        } else if (exten.equalsIgnoreCase(".jpg")) {
            return "image/jpeg";
        } else {
            return "application/octet-stream";
        }
    }

    public void Log(boolean in_window, String s) {
        if (in_window) {
            root.Log("" + client.getInetAddress().getHostAddress() + ";"
                    + client.getPort() + "  " + s);
        } else {
            System.out.print("" + client.getInetAddress().getHostAddress()
                    + ";" + client.getPort() + "  " + s);
        }
    }

    /**
     * Loads a JavaServlet class into the VM name : "Classname.prlt"
     */
    private JavaEPOST start_POSTserver(String name) {
        // Removes the trailing ".prlt" from the name
        name = name.substring(name.lastIndexOf(java.io.File.separatorChar) + 1, name.length() - 5);
        JavaEPOST post = null;
        try {
            Class postClass = Class.forName(name);
            Object postObject = postClass.newInstance();
            post = (JavaEPOST) postObject;
        } catch (ClassNotFoundException e) {
            System.err.println("POST class not found:" + e);
        } catch (InstantiationException e) {
            System.err.println("POST class instantiation:" + e);
        } catch (IllegalAccessException e) {
            System.err.println("POST class access:" + e);
        }
        return post;
    }

    @Override
    public void run() {

        HTTPAnswer ans = null;   // HTTP answer object
        PrintStream pout = null;
        String ver = "HTTP/1.1";
        HTTPRequest req;
        int timeout = 0;
        boolean controlVar = false;
        String request;

        try {
            InputStream in = client.getInputStream();
            BufferedReader bin = new BufferedReader(
                    new InputStreamReader(in, "8859_1"));
            OutputStream out = client.getOutputStream();
            pout = new PrintStream(out, false, "8859_1");

            do {

                try {
                    if ((request = bin.readLine()) == null) {
                        break;
                    }

                    Log(true, "Request: " + request + "\n");
                    StringTokenizer st = new StringTokenizer(request);
                    if (st.countTokens() != 3) {
                        return;  // Invalid request
                    }

                    String code = st.nextToken();    // USES HTTP syntax
                    String file = st.nextToken();    // for requesting files
                    ver = st.nextToken();

                    req = new HTTPRequest();

                    while ((request = bin.readLine()).length() > 0) {
                        //Log(true, "Request: " + request + "\n");
                        String[] spliter = request.split(":", 2);
                        req.PutValue(spliter[0].trim(), spliter[1].trim());
                    }

                    ans = new HTTPAnswer(root,
                            client.getInetAddress().getHostAddress() + ":" + client.getPort(),
                            guiHttpd.server_name + " - " + InetAddress.getLocalHost().getHostName() + "-" + root.server.getLocalPort());

                    //Validaçao de implementaçao
                    if (code.equalsIgnoreCase(NameDictionary.HEAD) || code.equalsIgnoreCase(NameDictionary.GET)
                            || code.equalsIgnoreCase(NameDictionary.POST) || code.equalsIgnoreCase(NameDictionary.GETPOST)) {

                        ans.set_version(ver);
                        ans.set_Date();

                        if (req.GetValue(NameDictionary.Connection).equalsIgnoreCase(NameDictionary.KeepAlive)
                                && (timeout = root.getKeepAlive()) > 0) {

                            Log(true, "TIMEOUT Active: " + timeout + "\n");
                            ans.set_KeepAlive(timeout);
                            client.setSoTimeout(timeout);
                            controlVar = true;

                        } else {
                            controlVar = false;
                        }

                        if (IsPRLT(file)) {
                            JavaEPOST prlt = start_POSTserver(PRLTfileName(file));

                            if (code.equalsIgnoreCase(NameDictionary.GET)) {
                                prlt.doGet(client, req.Requests, null, ans);
                            }

                            if (code.equalsIgnoreCase(NameDictionary.POST)) {
                                System.out.println("NOT YET IMPLEMENTED");
                            }

                        } else {

                            String filename = root.getRaizHtml() + file + (file.equals("/") ? "index.htm" : "");
                            System.out.println("Filename= " + filename);
                            File f = new File(filename);

                            if (f.exists() && f.isFile()) {

                                ans.set_file(new File(filename), guessMime(filename));

                                if (req.IsModified(f)) {
                                    ans.set_code(HTTPReplyCode.NOTMODIFIED);
                                } else {
                                    ans.set_code(HTTPReplyCode.OK);
                                }

                            } else {
                                System.out.println("File not found");
                                ans.set_error(HTTPReplyCode.NOTFOUND, ver);
                            }
                        }

                    } else {
                        System.out.println("NOT IMPLEMENTED");
                        ans.set_error(HTTPReplyCode.NOTIMPLEMENTED, ver);
                        controlVar = false;
                    }

                    //SEND REPLY
                    if (code.equalsIgnoreCase(NameDictionary.HEAD)) {
                        ans.send_Answer(pout, false, true);

                    } else {
                        ans.send_Answer(pout, true, true);
                    }
                } catch (SocketTimeoutException e) {
                    Log(true, "Timeout expired! \n");
                    controlVar = false;
                } catch (SocketException e) {
                    System.err.println("Erro no keep alive: " + e);
                }

            } while (controlVar);

            in.close();
            pout.close();
            out.close();
        } catch (IOException e) {
            if (root.active()) {
                System.out.println("I/O error " + e);
                e.printStackTrace(System.err);
            }
        } finally {
            try {
                client.close();
            } catch (IOException e) {
                // Ignore
                System.out.println("Error closing client" + e);
            }
            root.thread_ended();
            Log(true, "Closed TCP connection\n");
        }
    }

    private Boolean IsPRLT(String file) {
        String[] splitter = file.split("\\.");
        if (splitter.length != 2) {
            return false;
        }
        return splitter[1].trim().equalsIgnoreCase(NameDictionary.PRLT);
    }

    private String PRLTfileName(String file) {
        return file.replace("/", "");
    }

}
