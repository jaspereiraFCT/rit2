
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;



/**
 *
 * @author Joaquim
 */
public class HTTPRequest {

    public Properties Requests;

    public HTTPRequest() {
        Requests = new Properties();
    }

    public boolean PutValue(String key, String value) {
        if (Requests.contains(key.toLowerCase())) {
            return false;
        }
        Requests.put(key.toLowerCase(), value);
        return true;
    }

    public String GetValue(String key) {
        return Requests.getProperty(key.toLowerCase());
    }

    public Boolean IsModified(File f) {

        Boolean IsModified = false;
        Boolean IsETAGValid = false;

        if (Requests.containsKey(NameDictionary.IfModifiedSince.toLowerCase())) {
            DateFormat httpformat = new SimpleDateFormat("EE, d MMM yyyy HH:mm:ss zz", Locale.UK);
            httpformat.setTimeZone(TimeZone.getTimeZone("GMT"));

            IsModified = httpformat.format(f.lastModified()).
                    equals(Requests.get(NameDictionary.IfModifiedSince.toLowerCase()));
        }

        if (Requests.containsKey(NameDictionary.IfNoneMatch.toLowerCase())) {
            IsETAGValid = Requests.getProperty(NameDictionary.IfNoneMatch.toLowerCase()).
                    equalsIgnoreCase(Integer.toString(f.hashCode()));
        }

        return (IsModified && IsETAGValid);
    }

    // JUST FOR DEBIUGING
    public void View() {
        Iterator<Object> it = Requests.keySet().iterator();
        int aux = 1;
        System.out.println("<--------------------------------Teste------------------------------>");
        while (it.hasNext()) {
            String something = it.next().toString();
            System.out.println(aux + " " + something + " " + Requests.getProperty(something));
            aux++;
        }
        System.out.println("<--------------------------------Teste------------------------------>");
    }
}
