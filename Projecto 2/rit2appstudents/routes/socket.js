/**
 * This module deals with communications from a client via the WebSocket for real time communication
 */
var usermodel = require('../models/user.js'); // model for the database data this Object is used to access the database

//export function for listening to the socket
module.exports = function (io,socketioJwt,secret) {  

	var socketsID =[];

	io.set('authorization', socketioJwt.authorize({ // demand the signed token in the WebSocket handshake
		secret: secret,
		handshake: true
	}));	

	io.sockets.on('connection', function (socket) {  // first time it is called is when the client connects sucessfully

		var remove = function(origArr,username){
			var origLen = origArr.length,
			x, found= false;

			for(x = 0 ; x < origLen; x++){
				if(origArr[x].username === username){
					found=true;
					break;
				}			
			}
			if(found){
				origArr.splice(x,1);
			}

			return origArr;
		};

		var search = function(origArr, username){
			var origLen = origArr.length,
			x, found= false;

			for(x = 0 ; x < origLen; x++){
				if(origArr[x].username === username){
					return origArr[x].socketID;
				}			
			}

			return null;
		};

		socketsID.push({socketID: socket.id, username: socket.handshake.decoded_token.username});
		console.log(socket.handshake.decoded_token.username, 'connected'); // shows username in the valid token sent by client


		//defintion and handling of events
		//new user event sent by client
		socket.on('newUser:username', function (data) {    	  		  
			//check database to see if user already logged
			usermodel.find({islogged: true, username:{$ne : data.username}},{username:1, _id:0}, function(err, onlineUsers){ 
				if (err) {                               
					console.error(err);   
				} else { // sucess send the array of online users        
					if(onlineUsers != ""){
						console.log(onlineUsers); //onlineUsers contains the result 
						socket.emit('init', onlineUsers);
						socket.broadcast.emit('user:join', data);  	  	  		
						console.log ("new user event received");
					}
					else{
						console.log("no Users Online");
					}
				}
			});    // end user model update

		});

		// user's message event
		socket.on('send:message', function (data) {
			// search the socket id of destination user
			// send a JSON object with message and other needed data to the destiny socket
			var socketID = search(socketsID,data.to);
			if(socketID){
				io.sockets.socket(socketID).emit('send:message', data);	
			}

		});


//		/  TODO ADD other event handling code suggestions:

		socket.on('user:coord', function (data) { 
			var socketID = search(socketsID,data.to);
			if(socketID){
				io.sockets.socket(socketID).emit('user:coordCheck', data);	
			}
		});


//		user logout event broadcast it to other users and update database
		socket.on('user:logout', function (data) { 
			usermodel.update({username: data.username},{$set:{islogged: false}},function(err, User){ 
				if (err) { 
					console.error(err);       
				}else{         
					console.log(User);
					socketsID = remove(socketsID,data.username);
					socket.broadcast.emit('user:out', data); 
				}
			});
		});

		socket.on('disconnect', function(){
			var index = socketsID.map(function(e) { return e.socketID; }).indexOf(socket.id);
			
			usermodel.update({username: socketsID[index].username},{$set:{islogged: false}},function(err, User){ 
				if (err) { 
					console.error(err);       
				}else{         
					console.log(User);
				}
			});
			socket.broadcast.emit('user:disconected', {username: socketsID[index].username}); 
			socketsID.splice(index,1);
			
		});

	});
}